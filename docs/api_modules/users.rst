.. This file has been autogenerated by generate_modules.py


Users
=====

.. toctree::
   :maxdepth: 1

.. automodule:: pyeapi.api.users
   :members:
   :undoc-members:
   :show-inheritance:
