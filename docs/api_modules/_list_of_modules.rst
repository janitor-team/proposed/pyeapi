.. This file has been autogenerated by generate_modules.py


Api
===

.. toctree::
   :maxdepth: 2

   abstract
   acl
   bgp
   interfaces
   ipinterfaces
   mlag
   ntp
   ospf
   routemaps
   spanningtree
   staticroute
   stp
   switchports
   system
   users
   varp
   vlans
   vrfs
   vrrp
